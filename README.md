# HydrophobicMicrobeads

This repository contains the raw data and the analysis of the study:

> Hydrophobic Microbeads for the detection of antiphospholipid antibodies in human sera

by Franziska Dinter^[Brandenburgische Technische Universität Cottbus-Senftenberg, Universitätsplatz 1, 01968 Senftenberg, Germany], Thomas Thiele^[PolyAn GmbH, Schkopauer Ring 6, 12681 Berlin, Germany], Uwe Schedler^[PolyAn GmbH, Schkopauer Ring 6, 12681 Berlin, Germany], Werner Lehmann^[attomol GmbH, Schulweg 6, 03205 Bronkow, Germany], Peter Schierack^[Brandenburgische Technische Universität Cottbus-Senftenberg, Universitätsplatz 1, 01968 Senftenberg, Germany], Stefan Rödiger^[Brandenburgische Technische Universität Cottbus-Senftenberg, Universitätsplatz 1, 01968 Senftenberg; Germany, CreativeOpenLab (COLab), Siemens-Halske-Ring 2, 03044 Cottbus, Germany; Faculty of Health Sciences, joint Faculty of the Brandenburg University of Technology Cottbus – Senftenberg, the Brandenburg Medical School Theodor Fontane and the University of Potsdam, Berlin, Germany]
